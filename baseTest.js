import {puppeteer} from "./pageObject.spec";

export var browser;
export var page;

export let setUp = async () =>{
  const $browser =  await puppeteer.launch({headless: false});
  const $page = await $browser.newPage();
  browser = $browser;
  page = $page;
  await page.goto('https://www.amazon.com/');
}

export let closeBrowser = async () => {
  await browser.close();
}