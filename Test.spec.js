import {searchWithKeyword} from "./pageObject.spec";
import {getArrWithSearchResults} from "./pageObject.spec";
import {closeBrowser, setUp} from "./baseTest";


function checkIfInList(arr) {
  for (var i = 0; i< arr.length; i++) {
    if(arr[i] === 'Head First Java, 2nd Edition'){return true;}
  }
  return false;
}
beforeAll(async () =>{
  await setUp();
});

test('Checking if Head First Java is in list', async () => {
  await searchWithKeyword();
  var arr = await getArrWithSearchResults();
  await console.log(arr)
  await expect(checkIfInList(arr)).toBeTruthy();
}, 30000);

afterAll(async () =>{
  await closeBrowser();
});