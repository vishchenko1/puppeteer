import{page} from "./baseTest";

export const puppeteer = require('puppeteer');

export let getArrWithSearchResults = async () => {
  const bookNames = await page.$$('.a-size-mini.a-spacing-none.a-color-base.s-line-clamp-2');
  const authorNames = await page.$$('div > .a-row.a-size-base.a-color-secondary > .a-size-base.a-link-normal');
  var arr = [];
  for (var i = 0; i < 16; i++) {
    const name = await page.evaluate(book => {
      return book.innerText;
    }, bookNames[i]);
    const author = await page.evaluate(author => {
      return author.innerText;
    }, authorNames[i]);
    arr.push(author, name);
  }
  return arr;
};

export let searchWithKeyword = async () => {
  await page.type('#twotabsearchtextbox', 'Java');
  const submitButton = '.nav-search-submit.nav-sprite';
  await page.click(submitButton);
  await page.waitForSelector('.a-size-mini.a-spacing-none.a-color-base.s-line-clamp-2');
}



